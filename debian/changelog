sqlheavy (0.1.1-2.2+pantheon1) jessie; urgency=medium

  * Reimport from snapshots.debian.org

 -- Nicolas Bruguier <gandalfn@club-internet.fr>  Thu, 15 Jan 2015 01:25:36 +0100

sqlheavy (0.1.1-2.2) unstable; urgency=medium

  * Non-maintainer upload.
  * Run dh-autoreconf to update config.{sub,guess} and {libtool,aclocal}.m4
    to fix FTBFS on arm64 (Closes: #727509).
    - add autotools-dev to Build-Depends in debian/control
    - add "--with autotools_dev" when invoking dh in debian/rules

 -- John Paul Adrian Glaubitz <glaubitz@physik.fu-berlin.de>  Tue, 01 Jul 2014 17:08:37 +0200

sqlheavy (0.1.1-2.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Upload to unstable.
    - the fixed install path for the vapi files in version 0.1.1-2
      is required for the reverse dependencies of sqlheavy to build
  * Update Standards-Version to 3.9.5:
    - drop obsolete DM-Upload-Allowed field from debian/control

 -- John Paul Adrian Glaubitz <glaubitz@physik.fu-berlin.de>  Tue, 01 Jul 2014 14:27:43 +0200

sqlheavy (0.1.1-2) experimental; urgency=low

  * Set debian/compat to 9.
  * debian/libsqlheavy*-dev.install: install usr/share/vala/vapi/* files
    in usr/share/vala/vapi directory.
  * debian/control: require debhelper (>= 9.20120417) in Build-Depends field.
  * debian/control: add dh-exec (>= 0.3) to Build-Depends field.
  * debian/{libsqlheavy*-dev,libsqlheavy*-0}.install: modify usr/lib to
    usr/lib/${DEB_HOST_MULTIARCH} and run dh-exec.
  * debian/control: add Pre-Depend: multiarch-support in libsqlheavygtk0.1-0
    and libsqlheavy0.1-0 packages.
  * debian/control: add Multi-Arch fields.

 -- Devid Antonio Filoni <d.filoni@ubuntu.com>  Mon, 02 Jul 2012 07:42:48 +0200

sqlheavy (0.1.1-1) unstable; urgency=low

  * New upstream release (Closes: #663320).
  * Fix debug-package-should-be-priority-extra lintian warning.
  * debian/control: switch to vala-0.16 in Build-Depends field.
  * debian/libsqlheavy-dev.install, debian/libsqlheavygtk-dev.install:
    install files in vala-0.16 dir.
  * Update libsqlheavy0.1-0.symbols.amd64 file.
  * debian/rules: update override_dh_makeshlibs target.
  * Bump Standards-Version to 3.9.3.

 -- Devid Antonio Filoni <d.filoni@ubuntu.com>  Thu, 17 May 2012 09:47:17 +0200

sqlheavy (0.1.0-1) unstable; urgency=low

  * Initial release (Closes: #618032).

  [ Luca Falavigna ]
  * Enable DM upload.

 -- Devid Antonio Filoni <d.filoni@ubuntu.com>  Sun, 23 Oct 2011 15:43:22 +0200
